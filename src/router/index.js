
import { createRouter, createWebHashHistory } from "vue-router"
 
const routes = [
    {
        path: '/',
        name: 'index',
        component: () => import('@/views/index.vue')
    },
]
export const router = createRouter({
    history: createWebHashHistory(),
    routes: routes
})
 
export default router